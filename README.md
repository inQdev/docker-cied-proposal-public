CIED Dockerized Platform
========================

The goal of this repo is to provide a common development platform, easy to deploy and use

# Requirements

- Docker: https://docs.docker.com/engine/installation/. Installation depends on the platform but it's pretty straighforward, in MacOs you must install Toolbox, it contains all what's needed to run the app (Docker, Docker Compose)
- Docker Compose: For Linux machines, Docker Compose must be installed manually: https://docs.docker.com/compose/install/
- NOTE: Docker Compose is not supported on Windows yet

# Installation

## The following two steps are for OS X users only!

1. Install [VirtualBox lastest release](https://www.virtualbox.org/wiki/Downloads)
2. Once installed, run the following commands (keep in mind the name you assign to the machine)

```bash
$ docker-machine create --driver virtualbox MACHINE_NAME
$ docker-machine start MACHINE_NAME
$ eval $(docker-machine env MACHINE_NAME)
```

_These two commands need to be executed everytime you are going to work._


First, clone this repository:

```bash
$ git clone git@bitbucket.org:inqbation/docker-cied-proposal.git
```

Then, run:

```bash
$ docker-compose up
```

After the installation, you can visite your Symfony application on the following URL: `http://yourip:8880`

_OS X users: run `docker-machine ip MACHINE_NAME` to get the `yourip` part._

_Note :_ you can rebuild all Docker images by running:

```bash
$ docker-compose build
```

## Note:

In my instance the `docker-compose up` is returning an error after setting up the DB container:

```bash
db_1          | Version: '5.6.28'  socket: '/var/run/mysqld/mysqld.sock'  port: 3306  MySQL Community Server (GPL)
Gracefully stopping... (press Ctrl+C again to force)
Stopping ciedproposal_nginx_1 ... done
Stopping ciedproposal_php_1 ... done
Stopping ciedproposal_db_1 ... done
Stopping ciedproposal_application_1 ... done
ERROR: Couldn't connect to Docker daemon - you might need to run `docker-machine start default`.
```

*Disregard that message, it does not prevent the environment to run (see the start command below)*

# How it works?

Here are the `docker-compose` built images:

* `application`: This is the Symfony application code container,
* `db`: This is the MySQL database container (can be changed to postgresql or whatever in `docker-compose.yml` file),
* `php`: This is the PHP-FPM container in which the application volume is mounted,
* `nginx`: This is the Nginx webserver container in which application volume is mounted too,

This results in the following running containers:

```bash
> $ docker-compose ps
        Name                      Command               State              Ports
        -------------------------------------------------------------------------------------------
        docker_application_1   /bin/bash                        Up
        docker_db_1            /entrypoint.sh mysqld            Up      3306/tcp
        docker_nginx_1         nginx                            Up      443/tcp, 0.0.0.0:8880->80/tcp
        docker_php_1           php5-fpm -F                      Up      9000/tcp
```

You can start the environment (all the containers) running:

```bash
$ docker-compose start
```

And you can shut them down running:

```bash
$ docker-compose stop
```

# Read logs

You can access Nginx and Symfony application logs in the following directories into your host machine:

* `logs/nginx`
* `logs/symfony`

# About Networking

Once Docker has been installed, a new virtual network device is installed, and your containers will have IP addresses in the same network, for instance:

```bash
> $ ifconfig
docker0   Link encap:Ethernet  HWaddr 02:42:9d:52:e7:e5
          inet addr:172.17.0.1  Bcast:0.0.0.0  Mask:255.255.0.0
          inet6 addr: fe80::42:9dff:fe52:e7e5/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:269 errors:0 dropped:0 overruns:0 frame:0
          TX packets:101 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:0
          RX bytes:25375 (25.3 KB)  TX bytes:15423 (15.4 KB)

eth0      Link encap:Ethernet  HWaddr 08:00:27:81:7d:d7
          inet addr:192.168.11.138  Bcast:192.168.11.255  Mask:255.255.255.0
          inet6 addr: fe80::a00:27ff:fe81:7dd7/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:52816 errors:0 dropped:0 overruns:0 frame:0
          TX packets:29930 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:64327972 (64.3 MB)  TX bytes:2995345 (2.9 MB)
```

The `docker-compose ps` command doesn't show the IP address of a container, to know it, run:

```bash
$ docker inspect --format '{{ .NetworkSettings.IPAddress }}' "docker-container-name"
```

You can also create a function in your `.profile` file so that you have a simple command:

```bash
# Function to get Docker containers IP addresses
docker-ip() {
  docker inspect --format '{{ .NetworkSettings.IPAddress }}' "$@"
}
```

Now you can run:

```bash
$ docker-ip docker-container-name
```

# Symfony app configuration

Once you have everything in place, go to the `symfony` folder and run composer:

```bash
$ composer install
``` 

This will ask you for some values to create the parameters.yml file:

```bash
Creating the "app/config/parameters.yml" file
Some parameters are missing. Please provide them.
database_host (127.0.0.1): db
database_port (null):
database_name (symfony): symfony
database_user (root): symfony
database_password (null): 1nqb4t10n

mailer_host (127.0.0.1):
mailer_user (null):
mailer_password (null):
secret (ThisTokenIsNotSoSecretChangeIt): randomandoddstring
```

Please use the values shown here for the database connection, `db` is a value that Docker configures internally in the containers (got from `docker-compose.yml` file) that allows an easy way to connect other containers to the DB independent of the IP addressed assigned when the containers are started

SMTP configuration can depend on every developer's own environment and it won't be handled in this document

Once `composer` finishes, you can use your browser to connect to the port 8880 of your machine (the host): 

![cied-symfony-running-docker.png](https://bitbucket.org/repo/gqyMjn/images/629165584-cied-symfony-running-docker.png)